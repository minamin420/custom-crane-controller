﻿using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;

public class ArduinoControl : MonoBehaviour
{
    [SerializeField]
    bool isConnected;
    [SerializeField]
    string arduinoPort;
    [SerializeField]
    int baudRate;
    [SerializeField]
    SerialPort dataStream;

    public List<string> controllerData;

    public static ArduinoControl instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        dataStream = new SerialPort(arduinoPort, baudRate);
        dataStream.Open();

    }

    // Update is called once per frame
    void Update()
    {
        if (dataStream.IsOpen)
        {
            try
            {
                string[] data = dataStream.ReadLine().Split(',');

                controllerData[0] = data[0]; //Y1
                controllerData[1] = data[1]; //Y2
                controllerData[2] = data[2]; //btn
                Debug.Log(controllerData[0] + " , " + controllerData[1] + " , " + controllerData[2]);
            } catch (System.Exception)
            {

            }
        } else
        {
            dataStream.Close();
            isConnected = false;
        }
    }
}
