﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraneControl : MonoBehaviour
{
    public float rotateSpeed;
    public float armSpeed;
    public GameObject craneArm;
    public GameObject craneHand;
    Rigidbody rb;
    public ArduinoControl arduino;
    [SerializeField] private int rotateValue;
    [SerializeField] private int armValue;
    [SerializeField] private int btnValue;
    // Start is called before the first frame update
    void Start()
    {
        rb = craneArm.GetComponent<Rigidbody>();

    }
    // Update is called once per frame
    void Update()
    {
        CraneHandDown();
        CraneRotate();
        MoveArm();
    }

    public void CraneHandDown()
    {
        btnValue = int.Parse(ArduinoControl.instance.controllerData[2]);
        if (Input.GetKeyDown(KeyCode.Space) || btnValue == 1)
        {
            craneHand.GetComponent<Animator>().Play("crane down");
        }
    }
    public void CraneRotate()
    {
        rotateValue = int.Parse(ArduinoControl.instance.controllerData[0]);

        if (Input.GetKey(KeyCode.A) || rotateValue == -10)
        {
            transform.Rotate(-Vector3.up * rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D) || rotateValue == 10)
        {
            transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
        }
    }
    public void MoveArm()
    {
        armValue = int.Parse(ArduinoControl.instance.controllerData[1]);
        if (Input.GetKey(KeyCode.W) || armValue == 10)
        {
            rb.velocity = transform.forward * armSpeed;
        }
        if (Input.GetKey(KeyCode.S) || armValue == -10)
        {
            rb.velocity = -transform.forward * armSpeed;
        }
    }
}
