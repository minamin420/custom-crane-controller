#define joyY1 A0
#define joyY2 A2
#define BTN 2

unsigned long prevMillis = 0;
int msDelay = 0;

void setup() {
  Serial.begin(9600); 
  
  //pinMode(joyX1, INPUT);
  pinMode(joyY1, INPUT);
  //pinMode(joyX2, INPUT);
  pinMode(joyY2, INPUT);
  pinMode(BTN, INPUT_PULLUP); 
  
}

void loop() {
  unsigned long currentMillis = millis();
  if(currentMillis - prevMillis > msDelay){
    inputLoop();
    
    prevMillis = currentMillis;
  }
}

void inputLoop(){
  int joyRY1 = analogRead(joyY1);
  joyRY1 = map(joyRY1, 1, 1024, 10, -11);
  int joyRY2 = analogRead(joyY2);
  joyRY2 = map(joyRY2, 1, 1024, 10, -11);

  int BTN_state = digitalRead(BTN);
  
  Serial.print(joyRY1);
  Serial.print(",");
  Serial.print(joyRY2);
  Serial.print(",");
  
  Serial.println(!BTN_state);
}
